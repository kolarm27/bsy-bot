# BSY bot

## Client

 - Runs once a minute.
 - Reads any new comments, executes commands.
 - Posts an "I'm alive" comment on each run.

## Controller

 - Posts/reads comments depending on parameters.

### Instructions

```bash
# request list of active users
python src/controller/main.py w

# request a directory listing
python src/controller/main.py ls path

# request user information
python src/controller/main.py id

# request copy of a remote file
python src/controller/main.py copy remote-file

# execute a command and get its output
python src/controller/main.py exec "command with args"
```

All instructions return a unique command identifier once submitted. To obtain the output, run:

```bash
# print outputs of the command from all clients
python src/controller/main.py status id
```

Additional management commands:

```bash
# list clients active in the past minute
python src/controller/main.py ping

# delete all comments in the gist
python src/controller/main.py clear
```

## Communication

The commands are encoded as links to fox images. Each image identifies one command and parameters are added as query and fragment parts of the URL.

The client results have the following form:

```
<!--client id-->
<!--command id-->
<!--base64 encoded image with steganography-hidden text-->
dog image link
```

The first three parts are the actual communication. The linked image is what is actually visible/rendered by GitHub.

*Note that I used steganography because I initially wanted the images to be visible, but it turned out GitHub doesn't allow uploading images via its API, only via the web UI. That's why the images with output are base64 encoded and embedded as text. The same image, without output, is also linked from an external source and is the only visible part of the comment.*

## Known issues

 - The random images used for hiding output are sometimes bigger than what GitHub allows in a comment. Posting will fail in that case.
 - Size of copied files is limited for the same reason.
